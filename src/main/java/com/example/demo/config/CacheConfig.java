//package com.example.demo.config;
//
//import com.example.demo.Model.Student;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.CachePut;
//import org.springframework.cache.annotation.Cacheable;
//
//@org.springframework.cache.annotation.CacheConfig(cacheNames = "student")
//public class CacheConfig {
//
//    @Cacheable
//    public int getId(Student student){
//        return student.getId();
//    }
//
//    @CachePut(key = "#student.id")
//    public Student updateStudent(int id,Student student){
//        return student;
//    }
//
//    @CacheEvict(key = "#student.id")
//    public int deleteStudent(int id){
//        return id;
//    }
//}
